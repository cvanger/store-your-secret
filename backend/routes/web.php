<?php

use App\Http\Controllers\SecretController;
use App\Model\Secret;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Routing\Router;

/** @var $router Router */
$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/secret/{hash}', function (Request $request, string $hash) {
    /** @var SecretController $secretController */
    $secretController = app(SecretController::class);

    $format = $request->header('Accept');
    if (!$format) {
        $format = 'json';
    }

    $status = Response::HTTP_OK;
    $headers = [];

    try {
        $secret = $secretController->getSecret($hash);
        $headers['Content-Type'] = $secretController->getContentType($format);

        $content = $secretController->getFormattedContent($secret, $format);
    } catch (Exception $exception) {
        $status = Response::HTTP_NOT_FOUND;
        $content = ['error' => 'Secret not found'];
    }

    return response($content, $status, $headers);
});

$router->post('/secret', function (Request $request) {

    try {
        $input = $request->json()->all();
        Validator::make($input, [
            SecretController::INPUT_SECRET_TEXT => 'required',
            SecretController::INPUT_EXPIRE_AFTER_VIEWS => 'required|numeric|gt:0',
            SecretController::INPUT_EXPIRE_AFTER => 'required|numeric'
        ])
            ->validate();


        $secret = new Secret();
        $secret->secretText = $input[SecretController::INPUT_SECRET_TEXT];
        $secret->expiresAt = Carbon::now()->addSeconds($input[SecretController::INPUT_EXPIRE_AFTER]);
        $secret->remainingViews = $input[SecretController::INPUT_EXPIRE_AFTER_VIEWS];

        /** @var SecretController $secretController */
        $secretController = app(SecretController::class);

        $secret = $secretController->addSecret($secret);

        $content = $secret->toJson();

        return response($content, 200, ['Content-Type' => 'application/json']);

    } catch (ValidationException $e) {
        return response($e->errors(), 405);
    } catch (Exception $e) {
        return response($e->getMessage(), 500);
    }
});
