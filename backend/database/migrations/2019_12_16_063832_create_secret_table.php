<?php

use App\IO\SecretTable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSecretTable extends Migration
{

    public function up(): void
    {
        Schema::create(SecretTable::TABLENAME, function (Blueprint $table) {
            $table->string(SecretTable::FIELD_HASH, 16)->unique();
            $table->integer(SecretTable::FIELD_EXPIRE_AT);
            $table->integer(SecretTable::FIELD_CREATED_AT);
            $table->integer(SecretTable::FIELD_REMAINING_VIEWS);
            $table->text(SecretTable::FIELD_SECRET_TEXT);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists(SecretTable::TABLENAME);
    }
}
