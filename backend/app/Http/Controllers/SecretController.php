<?php

namespace App\Http\Controllers;

use App\Exceptions\ExpiredException;
use App\Exceptions\NoRemainingViewsException;
use App\IO\SecretTable;
use App\Logic\HashLogic;
use App\Model\Secret;
use Carbon\Carbon;
use Exception;
use InvalidArgumentException;

class SecretController extends Controller
{
    const INPUT_SECRET_TEXT = 'secretText';
    const INPUT_EXPIRE_AFTER_VIEWS = 'expireAfterViews';
    const INPUT_EXPIRE_AFTER = 'expireAfter';

    /** @var HashLogic */
    private $hashLogic;
    /** @var SecretTable */
    private $secretTable;

    /**
     * @param HashLogic $hashLogic
     * @param SecretTable $secretTable
     */
    public function __construct(HashLogic $hashLogic, SecretTable $secretTable)
    {
        $this->hashLogic = $hashLogic;
        $this->secretTable = $secretTable;
    }

    /**
     * @param string $hash
     * @return Secret
     * @throws ExpiredException
     * @throws NoRemainingViewsException
     */
    public function getSecret(string $hash): Secret
    {
        $secret = $this->secretTable->findSecret($hash);

        $this->assertExpire($secret);
        $this->assertRemainingViews($secret);

        $secret = $this->secretTable->decrementRemainingViews($secret);

        return $secret;
    }

    /**
     * @param Secret $secret
     * @return Secret
     * @throws Exception
     */
    public function addSecret(Secret $secret)
    {
        $hash = $this->hashLogic->getHash();

        $secret->hash = $hash;
        $secret->createdAt = Carbon::now();

        $this->secretTable->addSecret($secret);

        return $secret;
    }

    /**
     * @param Secret $secret
     * @throws ExpiredException
     */
    private function assertExpire(Secret $secret)
    {
        $now = Carbon::now();

        if ($secret->expiresAt->lessThan($now)) {
            throw new ExpiredException();
        }
    }

    /**
     * @param Secret $secret
     * @throws NoRemainingViewsException
     */
    private function assertRemainingViews(Secret $secret)
    {
        if ($secret->remainingViews === 0) {
            throw new NoRemainingViewsException();
        }
    }

    /**
     * @param Secret $secret
     * @param string $format
     * @return string
     */
    public function getFormattedContent(Secret $secret, string $format): string
    {
        switch ($format) {
            case 'json':
                return $secret->toJson();
            case 'xml':
                return $secret->toXml();
            default:
                throw new InvalidArgumentException('Cannot format to ' . $format);
        }
    }

    /**
     * @param string $format
     * @return string
     */
    public function getContentType(string $format)
    {
        if ($format === 'xml') {
            return 'application/xml';
        }

        return 'application/json';
    }
}
