<?php

namespace App\Model;

use Carbon\Carbon;
use SimpleXMLElement;

class Secret
{

    /** @var string */
    public $hash;
    /** @var string */
    public $secretText;
    /** @var Carbon */
    public $createdAt;
    /** @var Carbon */
    public $expiresAt;
    /** @var int */
    public $remainingViews;

    /**
     * @return string
     */
    public function toJson()
    {
        return json_encode($this);
    }

    /**
     * @return string
     */
    public function toXml()
    {
        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><Secret/>');
        $vars = get_object_vars($this);
        foreach ($vars as $property => $value) {
            $xml->addChild($property, $value);
        }

        return (string)$xml->asXML();
    }
}
