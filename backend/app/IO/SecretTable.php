<?php

namespace App\IO;

use App\Model\Secret;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use stdClass;

class SecretTable
{

    const FIELD_HASH = 'hash';
    const FIELD_EXPIRE_AT = 'expireAt';
    const FIELD_CREATED_AT = 'createdAt';
    const FIELD_REMAINING_VIEWS = 'remainingViews';
    const FIELD_SECRET_TEXT = 'secretText';
    const TABLENAME = 'secret';

    public function __construct()
    {
        // just for the easier usage, not for production
        $existsTable = Schema::hasTable(static::TABLENAME);
        if (!$existsTable) {
            Artisan::call('migrate');
        }
    }

    /**
     * @param string $hash
     * @return Secret
     */
    public function findSecret(string $hash): Secret
    {
        $res = DB::table(static::TABLENAME)
            ->where(static::FIELD_HASH, $hash)
            ->first();

        if (!$res) {
            throw new ModelNotFoundException();
        }

        return $this->build($res);
    }

    /**
     * @param Secret $secret
     * @return Secret
     */
    public function decrementRemainingViews(Secret $secret): Secret
    {
        $secret->remainingViews--;

        DB::table(static::TABLENAME)->where(static::FIELD_HASH, $secret->hash)->update([
            static::FIELD_REMAINING_VIEWS => $secret->remainingViews,
        ]);

        return $secret;
    }

    /**
     * @param Secret $secret
     * @return bool
     */
    public function addSecret(Secret $secret): bool
    {
        return DB::table('secret')->insert([
            static::FIELD_HASH => $secret->hash,
            static::FIELD_EXPIRE_AT => $secret->expiresAt->timestamp,
            static::FIELD_CREATED_AT => $secret->createdAt->timestamp,
            static::FIELD_REMAINING_VIEWS => $secret->remainingViews,
            static::FIELD_SECRET_TEXT => $secret->secretText,
        ]);
    }

    /**
     * @param stdClass $res
     * @return Secret
     */
    private function build(stdClass $res): Secret
    {
        $secret = new Secret();

        $secret->hash = $res->{static::FIELD_HASH};
        $secret->remainingViews = $res->{static::FIELD_REMAINING_VIEWS};
        $secret->createdAt = Carbon::createFromTimestamp($res->{static::FIELD_CREATED_AT});
        $secret->expiresAt = Carbon::createFromTimestamp($res->{static::FIELD_EXPIRE_AT});
        $secret->secretText = $res->{static::FIELD_SECRET_TEXT};

        return $secret;
    }
}
