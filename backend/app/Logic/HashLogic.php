<?php

namespace App\Logic;

use Exception;

class HashLogic
{
    /**
     * @return string
     * @throws Exception
     */
    public function getHash()
    {
        $hash = '';
        for ($i = 0; $i < 4; $i++) {
            $hash .= random_int(0, 9);
            $hash .= chr(random_int(65, 90));
            $hash .= chr(random_int(97, 122));
        }

        return str_shuffle($hash);
    }
}
