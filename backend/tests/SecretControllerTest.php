<?php

use App\Http\Controllers\SecretController;
use App\IO\SecretTable;
use App\Logic\HashLogic;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Laravel\Lumen\Testing\DatabaseMigrations;

class SecretControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        $now = 1576477583;
        Carbon::setTestNow(Carbon::createFromTimestamp($now));

        DB::table(SecretTable::TABLENAME)->insert([
            SecretTable::FIELD_HASH => 'abc111',
            SecretTable::FIELD_REMAINING_VIEWS => 2,
            SecretTable::FIELD_CREATED_AT => $now - 5,
            SecretTable::FIELD_EXPIRE_AT => $now + 30,
            SecretTable::FIELD_SECRET_TEXT => 'cat',
        ]);

        DB::table(SecretTable::TABLENAME)->insert([
            SecretTable::FIELD_HASH => 'abc222',
            SecretTable::FIELD_REMAINING_VIEWS => 0,
            SecretTable::FIELD_CREATED_AT => $now - 5,
            SecretTable::FIELD_EXPIRE_AT => $now + 10,
            SecretTable::FIELD_SECRET_TEXT => 'dog',
        ]);

        DB::table(SecretTable::TABLENAME)->insert([
            SecretTable::FIELD_HASH => 'abc333',
            SecretTable::FIELD_REMAINING_VIEWS => 2,
            SecretTable::FIELD_CREATED_AT => $now - 5,
            SecretTable::FIELD_EXPIRE_AT => $now - 1,
            SecretTable::FIELD_SECRET_TEXT => 'mouse',
        ]);
    }

    public function testClassCreation(): void
    {
        $secretController = app(SecretController::class);
        static::assertInstanceOf(SecretController::class, $secretController);
    }

    /**
     * @return array
     */
    public function getProvider()
    {
        $tests = [];

        $tests['success json'] = [
            'abc111',
            'json',
            '{"hash":"abc111","secretText":"cat","createdAt":"2019-12-16T06:26:18.000000Z","expiresAt":"2019-12-16T06:26:53.000000Z","remainingViews":1}',
        ];

        $tests['success xml'] = [
            'abc111',
            'xml',
            <<<'HEREDOC'
<?xml version="1.0" encoding="UTF-8"?>
<Secret><hash>abc111</hash><secretText>cat</secretText><createdAt>2019-12-16 06:26:18</createdAt><expiresAt>2019-12-16 06:26:53</expiresAt><remainingViews>1</remainingViews></Secret>

HEREDOC
            ,
        ];

        $tests['expired'] = [
            'abc222',
            'json',
            '{"error":"Secret not found"}',
        ];

        $tests['no remaining views'] = [
            'abc333',
            'json',
            '{"error":"Secret not found"}',
        ];

        $tests['not exists'] = [
            'abc444',
            'json',
            '{"error":"Secret not found"}',
        ];

        return $tests;
    }


    /**
     * @dataProvider getProvider
     * @param string $hash
     * @param string $accept
     * @param string $expected
     */
    public function testGetSecret(string $hash, string $accept, string $expected): void
    {
        $response = $this->json('GET', '/secret/' . $hash, [], ['Accept' => $accept])->response;
        $this->assertEquals($expected, $response->content());
    }

    public function failedPostProvider()
    {
        $tests = [];

        $tests['no secretText'] = [
            [
                'expireAfterViews' => 10,
                'expireAfter' => 12,
            ],
            '{"secretText":["The secret text field is required."]}'
        ];

        $tests['no expireAfterViews'] = [
            [
                'secretText' => 'mouse',
                'expireAfter' => 12,
            ],
            '{"expireAfterViews":["The expire after views field is required."]}'
        ];

        $tests['no expireAfter'] = [
            [
                'expireAfterViews' => 10,
                'secretText' => 'mouse',
            ],
            '{"expireAfter":["The expire after field is required."]}'
        ];

        $tests['zero expireAfterViews'] = [
            [
                'expireAfterViews' => 0,
                'expireAfter' => 12,
                'secretText' => 'mouse',
            ],
            '{"expireAfterViews":["The expire after views must be greater than 0."]}'
        ];

        return $tests;
    }

    /**
     * @dataProvider failedPostProvider
     * @param array $data
     * @param string $expectedResponse
     */
    public function testFailedPost(array $data, string $expectedResponse = null)
    {
        $response = $this->json('POST', '/secret', $data, ['Content-Type' => 'application/json'])->response;
        $content = $response->content();
        $status = $response->status();

        $this->assertTrue(is_string($content));
        $this->assertEquals($expectedResponse, $content);
        $this->assertEquals(405, $status);
    }

    public function testSuccessPost()
    {
        $mock = Mockery::mock(HashLogic::class);
        $mock->shouldReceive('getHash')->once()->andReturn('Aa1Bb2Cc3Dd4');

        $this->app->instance(HashLogic::class, $mock);

        $data = [
            'expireAfterViews' => 2,
            'expireAfter' => 12,
            'secretText' => 'mouse',
        ];
        $response = $this->json('POST', '/secret', $data)->response;
        $content = $response->content();
        $status = $response->status();

        $expected = '{"hash":"Aa1Bb2Cc3Dd4","secretText":"mouse","createdAt":"2019-12-16T06:26:23.000000Z","expiresAt":"2019-12-16T06:26:35.000000Z","remainingViews":2}';

        $this->assertEquals($expected, $content);
        $this->assertEquals(200, $status);
    }
}
