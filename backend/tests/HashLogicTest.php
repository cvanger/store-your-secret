<?php

use App\Logic\HashLogic;

class HashLogicTest extends TestCase
{

    public function testGetHash(): void
    {
        /** @var HashLogic $hashLogic */
        $hashLogic = app(HashLogic::class);

        $hash = $hashLogic->getHash();
        $this->assertEquals(12, strlen($hash));
    }

}
