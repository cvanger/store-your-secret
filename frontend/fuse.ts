import {CSSPlugin, FuseBox, FuseBoxOptions, SassPlugin, WebIndexPlugin} from 'fuse-box';

const plugins = [
    [SassPlugin(), CSSPlugin()],
    WebIndexPlugin({
        target: 'index.html',
        template: 'src/index.html',
        path: '',
    }),
];

const options: FuseBoxOptions = {
    homeDir: 'src',
    output: 'build/$name.js',
    alias: {
        component: '~/component',
        style: '~/style',
    },
    plugins,
};

const fuse = FuseBox.init(options);

fuse.bundle('app')
    .instructions(' > index.tsx')
    .hmr()
    .watch();

fuse.dev();

fuse.run();
