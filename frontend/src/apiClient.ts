import {CreateSecretData, GetSecretData, Secret} from './types.d';

import format from 'xml-formatter';

async function request(
    method: 'POST' | 'GET',
    body?: string,
    query = '',
    header?: Record<string, string>,
): Promise<Response> {
    const headers = {'Content-Type': 'application/json', ...header};

    const apiUrl = 'http://localhost:81/secret';

    return fetch(`${apiUrl}/${query}`, {
        method,
        body,
        headers,
    });
}

export async function postSecret(secret: CreateSecretData) {
    const response = await request('POST', JSON.stringify(secret));
    const text = await response.text();

    return (JSON.parse(text) as Secret).hash;
}

export async function getSecret(getSecretData: GetSecretData) {
    const response = await request('GET', undefined, getSecretData.hash, {Accept: getSecretData.contentType});

    const contentType = response.headers.get('Content-Type');

    if (response.status !== 200) {
        throw new Error();
    }

    if (!contentType) {
        throw new Error('No Content-Type');
    }

    switch (contentType) {
        case 'application/json':
            return JSON.stringify(await response.json(), undefined, 4);
        case 'application/xml':
            return format(await response.text());
        default:
            throw new Error(`Cannot handle Content-Type: ${contentType}`);
    }
}
