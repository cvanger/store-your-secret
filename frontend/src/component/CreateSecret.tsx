import {Field, FieldProps, Form, Formik,} from 'formik';
import React from 'react';
import {CreateSecretData} from '../types';
import {LabelBar, LabelBarProps} from './form/Labelbar';
import {ShowHash} from './ShowHash';
import {postSecret} from '../apiClient';
import {FormikTextInput} from 'component/form/FormikInput';

function validate(values: CreateSecretData) {
    const errors: Partial<{ [P in keyof CreateSecretData]: string }> = {};

    if (!values.secretText) {
        errors.secretText = 'required';
    }

    if (!Number.isInteger(values.expireAfterViews) || values.expireAfterViews < 1) {
        errors.expireAfterViews = 'must be greater than 0';
    }

    if (!Number.isInteger(values.expireAfter)) {
        errors.expireAfter = 'must be number';
    }

    return errors;
}

export class CreateSecret extends React.Component<{}, { hash?: string }> {
    constructor(props: {}) {
        super(props);

        this.state = {
            hash: '',
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    private async onSubmit(values: CreateSecretData) {
        const createdHash = await postSecret(values);
        this.setState({hash: createdHash});
    }

    render() {
        return (
            <div className="createSecret">
                <h2>Create Secret</h2>
                <Formik
                    initialValues={{
                        secretText: '',
                        expireAfterViews: 0,
                        expireAfter: 0,
                    }}
                    onSubmit={this.onSubmit}
                    validate={validate}
                >
                    <Form>
                        <SecretArea/>
                        <FormikTextInput name="expireAfterViews" label="Expires after views" type="number"/>
                        <FormikTextInput name="expireAfter" label="Expires after time" type="number"/>
                        <button type="submit">Submit</button>
                    </Form>
                </Formik>
                {this.state.hash && <ShowHash hash={this.state.hash}/>}
            </div>
        );
    }
}

const SecretArea = () => {
    const props = {
        name: 'secretText',
        label: 'Secret',
    };
    return (
        <Field
            name={props.name}
        >
            {({field, form, meta}: FieldProps) => {
                const labelBarProps: LabelBarProps = {
                    label: props.label,
                    touched: !!form.touched[props.name],
                    error: form.errors[props.name] ? String(form.errors[props.name]) : undefined,
                };
                return (
                    <div className="field">
                        <LabelBar {...labelBarProps} />
                        <textarea {...field} />
                    </div>
                );
            }}
        </Field>
    );
};
