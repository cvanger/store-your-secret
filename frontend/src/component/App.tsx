import {CreateSecret} from './CreateSecret'
import React from 'react';
import {GetSecret} from './GetSecret';

import '../style/index.scss';

export const App = () => (
    <div className="app">
        <CreateSecret/>
        <GetSecret/>
    </div>
);
