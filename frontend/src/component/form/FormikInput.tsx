import React from 'react';
import {Field, FieldProps} from 'formik';
import {LabelBar, LabelBarProps} from './Labelbar';

export function FormikTextInput(props: { name: string, label: string, type?: string }) {
    return (
        <Field
            name={props.name}
        >
            {({field, form, meta}: FieldProps) => {
                const labelBarProps: LabelBarProps = {
                    label: props.label,
                    touched: !!form.touched[props.name],
                    error: form.errors[props.name] ? String(form.errors[props.name]) : undefined,
                }
                return (
                    <div className="field">
                        <LabelBar {...labelBarProps} />
                        <input {...field} type={props.type ? props.type : 'text'}/>
                    </div>
                );
            }}
        </Field>
    );
}
