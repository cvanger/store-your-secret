import React from 'react';

export interface LabelBarProps {
    label: string;
    touched: boolean;
    error?: string;
}

export const LabelBar = (props: LabelBarProps) => {
    return (
        <div className="labelBar">
            <span className="label">{props.label}</span>
            {props.touched && props.error && <span className="error">{props.error}</span>}
        </div>
    );
};
