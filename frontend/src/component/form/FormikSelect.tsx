import { Field, FieldProps } from 'formik';
import * as React from 'react';
import { LabelBar, LabelBarProps } from './Labelbar';

export interface IOption {
    label: string;
    value: any;
}

export function FormikSelect(props: { name: string, options: IOption[], defaultValue?: any, label: string }) {
    return (
        <Field
            name={props.name}
        >
            {({ field, form }: FieldProps) => {
                const labelBarProps: LabelBarProps = {
                    label: props.label,
                    touched: !!form.touched[props.name],
                    error: form.errors[props.name] ? String(form.errors[props.name]) : undefined,
                }
                return (
                    <div className={'field'}>
                        <LabelBar {...labelBarProps} />
                        <select
                            defaultValue={props.defaultValue}
                            onChange={(event) => {
                                form.setFieldValue(props.name, event.target.value);
                            }}
                        >
                            {props.options.map((option) => (
                                <option key={option.value} value={option.value}>
                                    {option.label}
                                </option>
                            ))}
                        </select>
                    </div>
                )
            }}
        </Field>
    );
}
