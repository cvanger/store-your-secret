import React from 'react';

export const ShowSecret = (props: { secret: string }) => {
    return (
        <div className="showSecret">
            <p>Your secret:</p>
            <pre><code>{props.secret}</code></pre>
        </div>
    );
};
