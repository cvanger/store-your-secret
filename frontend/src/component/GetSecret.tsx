import {Form, Formik} from 'formik';
import React from 'react';
import {FormikTextInput} from './form/FormikInput';
import {FormikSelect} from './form/FormikSelect';
import {GetSecretData} from '../types';
import {ShowSecret} from './ShowSecret';
import {getSecret} from '../apiClient';

const contentTypeOptions = [
    {label: 'json', value: 'json'},
    {label: 'xml', value: 'xml'},
];

function validate(values: GetSecretData) {
    const errors: Partial<{ [P in keyof GetSecretData]: string }> = {};

    if (!values.hash) {
        errors.hash = 'required';
    }
    return errors;
}

export class GetSecret extends React.Component<{}, { secret?: string, error: boolean }> {
    constructor(props: {}) {
        super(props);

        this.state = {
            secret: '',
            error: false,
        };

        this.onSubmit = this.onSubmit.bind(this);
    }

    private async onSubmit(values: GetSecretData) {
        try {
            const secret = await getSecret(values);
            this.setState({
                secret,
                error: false,
            });
        } catch (e) {
            this.setState({
                error: true,
                secret: '',
            });
        }
    }

    public render() {
        return (
            <div>
                <h2>Get your Secret</h2>
                <Formik<GetSecretData>
                    initialValues={{
                        hash: '',
                        contentType: 'json',
                    }}
                    onSubmit={this.onSubmit}
                    validate={validate}
                >
                    <Form>
                        <FormikTextInput name="hash" label="Hash"/>
                        <FormikSelect name="contentType" label="Content type" options={contentTypeOptions}/>
                        <button type="submit">Get my secret</button>
                    </Form>
                </Formik>
                {this.state.secret && <ShowSecret secret={this.state.secret}/>}
                {this.state.error && <p>Secret not found</p>}
            </div>
        );
    }
}
