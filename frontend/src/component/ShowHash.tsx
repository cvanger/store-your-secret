import React from 'react';

export const ShowHash = (props: { hash: string }) => {
    return (
        <div className="showHash">
            <p>The hash to reveal the secret:</p>
            <pre>{props.hash}</pre>
        </div>
    );
};
