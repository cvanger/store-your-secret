export interface CreateSecretData {
    secretText: string,
    expireAfterViews: number,
    expireAfter: number,
}

export interface GetSecretData {
    hash: string,
    contentType: 'json' | 'xml',
}

export interface Secret {
    hash: string,
    secretText: string,
    createdAt: string,
    expiresAt: string,
    remainingViews: number,
}
