import * as ReactDom from 'react-dom';
import {App} from './component/App';
import React from 'react';

ReactDom.render(
    <App/>,
    document.getElementById('root'),
);
