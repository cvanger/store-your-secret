# StoreYourSecret

Docker mellett a docker-compose-t használtam, így az indítás: docker-compose up

Elérések:
* Frontend felület: http://localhost:4444
* Backend API: http://localhost:81/
* Adminer felület: http://localhost:8080/?server=mysql&username=root (jelszó: PaSsWoRd)

Amik nem fértek bele, de fontosnak tartanám, ha éles projekt lenne:
* külön user a db-ben
* frontend deploy:
  * production build (optimalizálás, treeshake)
  * statikus kiszolgálás
* frontend tesztelés

